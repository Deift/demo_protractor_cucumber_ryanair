var Actions = function(){

    var HomeActions = new require("./actions/HomeActions.js");
    var SelectFlightActions = new require("./actions/SelectFlightActions.js");
    var ExtrasActions = new require("./actions/ExtrasActions.js");

    this.homeActions = new HomeActions();
    this.selectFlightActions = new SelectFlightActions();
    this.extrasActions = new ExtrasActions();

};

module.exports = Actions;