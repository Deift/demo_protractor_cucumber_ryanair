var ExtrasActions = function () {

    var extrasPage = pages.extrasPage;

    this.addBags = function(bagsNumber){
        browser.wait(EC.presenceOf(extrasPage.btnAddBags()),3000);
        extrasPage.btnAddBags().click();
        function plusBags(nBags){
            if(nBags < bagsNumber) {
                extrasPage.listBtnPlusBags().get(0).click();
                return plusBags(nBags + 1);
            }
        }
        plusBags(0);
        return extrasPage.btnConfirmBags().click();
    };

    this.addSeats = function(seatsType){
        extrasPage.btnAddSeats().click();
        browser.wait(EC.presenceOf(extrasPage.listBtnSeat('standard').get(0)),3000);
        extrasPage.listBtnSeat(seatsType).get(0).click();
        extrasPage.btnNextSeats().click();
        browser.wait(EC.presenceOf(extrasPage.btnSameSeatsReturn()),3000).then(function(){
            extrasPage.btnSameSeatsReturn().click();
        },function(){
            extrasPage.listBtnSeat(seatsType).get(0).click();
            extrasPage.btnNextSeats().click();
        });
        return extrasPage.btnConfirmSeats().click();
    };

};

module.exports = ExtrasActions;