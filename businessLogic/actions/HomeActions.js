var HomeActions = function () {

    var homePage = pages.homePage;

    this.searchFlight = function (airportOut, airportIn, adults, teens, children, infants) {
        var dateOut = moment().add("10", "days").format('DD-MM-YYYY');
        var dateIn = moment().add("20", "days").format('DD-MM-YYYY');
        homePage.AirportOut().clear();
        homePage.AirportOut().sendKeys(airportOut);
        browser.actions().sendKeys(protractor.Key.ENTER).perform();
        homePage.AirportIn().clear();
        homePage.AirportIn().sendKeys(airportIn);
        browser.actions().sendKeys(protractor.Key.ENTER).perform();
        browser.wait(EC.presenceOf(homePage.dateOut(dateOut)),4000).then(function(){
            browser.executeScript('arguments[0].scrollIntoView()', homePage.dateOut(dateOut).getWebElement());
            return homePage.dateOut(dateOut).click();
        }, function(){
            homePage.calendarOut().click();
            browser.executeScript('arguments[0].scrollIntoView()', homePage.dateOut(dateOut).getWebElement());
            return homePage.dateOut(dateOut).click();
        });
        browser.wait(EC.presenceOf(homePage.dateIn(dateIn)),2000).then(function(){
            browser.executeScript('arguments[0].scrollIntoView()', homePage.dateIn(dateIn).getWebElement());
            return homePage.dateIn(dateIn).click();
        }, function(){
            homePage.calendarIn().click();
            browser.executeScript('arguments[0].scrollIntoView()', homePage.dateIn(dateIn).getWebElement());
            return homePage.dateIn(dateIn).click();
        });
        homePage.passegersDropdown().click();
        function recursePax(nAdults){
            if(nAdults < adults) {
                homePage.passegersAdults().click();
                return recursePax(nAdults + 1);
            }
        }
        recursePax(1);
        return homePage.btnLetsGo().click();
    };

};

module.exports = HomeActions;