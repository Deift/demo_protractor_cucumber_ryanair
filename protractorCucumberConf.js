exports.config = {
    onPrepare: function () {
        browser.manage().window().maximize();
        global.chai = require('chai');
        global.chaiAsPromised = require('chai-as-promised');
        chai.use(chaiAsPromised);
        global.expect = chai.expect;
        global.Faker = require('faker');
        global.EC = protractor.ExpectedConditions;
        global.moment = require('moment');

        baseUrl:env.baseUrl;
        browser.ignoreSynchronization = false;

        var Pages = new require('./businessLogic/Pages.js');
        global.pages = new Pages();
        var Actions = new require('./businessLogic/Actions.js');
        global.actions = new Actions();
    },
    directConnect: true,
    //seleniumAddress:'http://localhost:4444/wd/hub',
    baseUrl:'http://pfc-david.manujlopez.com/YALG/www/',
    framework: 'custom',
    frameworkPath: require.resolve('protractor-cucumber-framework'),

    specs: [
        './features/cucumber/features/*.feature'
    ],

    capabilities: {
        'browserName':'chrome',
        'version':'ANY'
    },
    allScriptsTimeout: 100000,

    baseUrl: env.baseUrl,
    cucumberOpts: {
        require: 'features/**/*.js',
        tags: '@dev',
        format: 'pretty'
    }
};
