module.exports = function JsonOutputHook() {
    var Cucumber = require('cucumber');
    var JsonFormatter = Cucumber.Listener.JsonFormatter();
    var fs = require('fs');


    JsonFormatter.log = function (json) {
        var fileName = "json_out/" + new Date().getTime() + "_output.json";
        fs.writeFile(fileName, json, function (err) {
            if (err) throw err;
            console.log("json file location: " + fileName);
        });
    };

    this.registerListener(JsonFormatter);
};