Feature: Adding extras to my booking
  As a user of Ryanair Web Application
  I should be able to add extras to my booking
  In order to purchase them

  Background:
    * I load Ryanair home page

  @dev
  Scenario Outline: Adding Bags
    Given I proceed to extras page
    When I add "<bags>" bags
    Then I should se message indicating bags added

    Examples:
      | bags |
      | 1    |
      | 2    |

  @dev
  Scenario Outline: Adding Seats
    Given I proceed to extras page
    When I add "<seats>" seats
    Then I should se message indicating seats added

    Examples:
      | seats    |
      | premium  |
      | standard |
