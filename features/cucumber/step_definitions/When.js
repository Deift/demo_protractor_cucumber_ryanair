module.exports = function () {

    this.When(/^I add "([^"]*)" seats$/, function (seatsType) {
        return actions.extrasActions.addSeats(seatsType);
    });

    this.When(/^I add "([^"]*)" bags$/, function (bagsNumber) {
        return actions.extrasActions.addBags(parseInt(bagsNumber));
    });


};
