module.exports = function() {

    this.Then(/^I load Ryanair home page$/, function () {
        return browser.get('https://www.ryanair.com/gb/en/');
    });

    this.Given(/^I proceed to extras page$/, function () {
        actions.homeActions.searchFlight('DUB', 'STN',3,0,0,0);
        return actions.selectFlightActions.selectFlight();
    });


};
