module.exports = function () {

    this.Then(/^I should se message indicating seats added$/, function () {
        return expect(pages.extrasPage.textSeatsAdded().isPresent()).to.eventually.be.true;
    });

    this.Then(/^I should se message indicating bags added$/, function () {
        return expect(pages.extrasPage.textBagsAdded().isPresent()).to.eventually.be.true;
    });

};
